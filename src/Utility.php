<?php

namespace Drupal\auto_translation;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Utility class for auto_translation module functions.
 *
 * @package Drupal\auto_translation
 */
class Utility {

  use StringTranslationTrait;
  use LoggerChannelTrait;

  /**
   * The config object.
   *
   * @var Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The http client object.
   *
   * @var GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The message interface object.
   *
   * @var Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The language interface object.
   *
   * @var Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The module handler object.
   *
   * @var Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a new Utility object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ClientInterface $http_client, MessengerInterface $messenger, LanguageManagerInterface $language_manager, ModuleHandlerInterface $module_handler) {
    $this->configFactory = $config_factory;
    $this->httpClient = $http_client;
    $this->messenger = $messenger;
    $this->languageManager = $language_manager;
    $this->moduleHandler = $module_handler;
  }

  /**
   * Dependency injection via create().
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('http_client'),
      $container->get('messenger'),
      $container->get('language_manager'),
      $container->get('module_handler')
    );
  }

  /**
   * Translates the given text from the source language to the target language.
   *
   * @param string $text
   *   The text to be translated.
   * @param string $s_lang
   *   The source language code.
   * @param string $t_lang
   *   The target language code.
   *
   * @return string|null
   *   The translated text or NULL if translation fails.
   */
  public function translate($text, $s_lang, $t_lang) {
    $config = $this->configFactory->get('auto_translation.settings');
    $provider = $config->get('auto_translation_provider') ?? 'google';
    $api_enabled = $config->get('auto_translation_api_enabled') ?? NULL;
    $translation = NULL;

    switch ($provider) {
      case 'google':
        $translation = $api_enabled ? $this->translateApiServerCall($text, $s_lang, $t_lang) : $this->translateApiBrowserCall($text, $s_lang, $t_lang);
        break;

      case 'libretranslate':
        $translation = $this->libreTranslateApiCall($text, $s_lang, $t_lang);
        break;

      case 'drupal_ai':
        if ($this->moduleHandler->moduleExists('ai') && $this->moduleHandler->moduleExists('ai_translate')) {
          $translation = $this->drupalAiTranslateApiCall($text, $s_lang, $t_lang);
        } else {
          $this->messenger->addError($this->t('AI translation module is not installed.'));
        }
        break;
    }

    return $translation;
  }

  /**
   * Translates the given text using the API libre translate server.
   *
   * @param string $text
   *   The text to be translated.
   * @param string $s_lang
   *   The source language of the text.
   * @param string $t_lang
   *   The target language for the translation.
   *
   * @return string
   *   The translated text.
   */
  public function libreTranslateApiCall($text, $s_lang, $t_lang) {
    $config = $this->configFactory->get('auto_translation.settings');
    $translation = NULL;
    $endpoint = 'https://libretranslate.com/translate';

    $options = [
      'headers' => ['Content-Type' => 'application/json'],
      'json' => [
        'q' => $text,
        'source' => $s_lang,
        'target' => $t_lang,
        'format' => 'text',
        'api_key' => $config->get('auto_translation_api_key'),
      ],
      'verify' => FALSE,
    ];

    try {
      $response = $this->httpClient->post($endpoint, $options);
      $result = Json::decode($response->getBody()->getContents());
      $translation = $result['translatedText'] ?? NULL;
    } catch (RequestException $e) {
      $this->getLogger('auto_translation')->error('Translation API error: @error', ['@error' => $e->getMessage()]);
    }

    return $translation;
  }

  /**
   * Translates the given text using the API Drupal AI translate server.
   *
   * @param string $text
   *   The text to be translated.
   * @param string $s_lang
   *   The source language of the text.
   * @param string $t_lang
   *   The target language for the translation.
   *
   * @return string
   *   The translated text.
   */
  public function drupalAiTranslateApiCall($text, $s_lang, $t_lang) {
    $logger = $this->getLogger('auto_translation');
    $translation = NULL;
    if (!$this->moduleHandler->moduleExists('ai') && !$this->moduleHandler->moduleExists('ai_translate')) {
      $logger->error('Auto translation error: AI Module not installed please install Drupal AI module');
      return [
        '#type' => 'markup',
        '#markup' => $this->t('AI Module not installed please install Drupal AI and Drupal AI Translate modules'),
      ];
    }
    if (!\Drupal::service('ai.provider')->hasProvidersForOperationType('chat', TRUE) && !\Drupal::service('ai.provider')->hasProvidersForOperationType('translate', TRUE)) {
      $markupError = $this->t('To use Drupal AI to translate you need to configure a provider for the operation type "translate" or "chat" in the <a href=":url" target="_blank">Drupal AI module providers section</a>.', [':url' => '/admin/config/ai/providers']);
      return [
        '#type' => 'markup',
        '#markup' => $markupError,
      ];
    }
    $container = $this->getContainer();
    $languageManager = $container->get('language_manager');
    $langFrom = $languageManager->getLanguage($s_lang);
    $langTo = $languageManager->getLanguage($t_lang);

    try {
      $translatedText = \Drupal::service('ai_translate.text_translator')->translateContent($text, $langTo, $langFrom);

      return $translatedText;
    } catch (RequestException $exception) {
      $logger->error('Auto translation error: @error', ['@error' => json_encode($exception->getMessage())]);
      $this->getMessages($exception->getMessage());
      return $exception;
    }
    return $translation;
  }

  /**
   * Translates the given text using the API server.
   *
   * @param string $text
   *   The text to be translated.
   * @param string $s_lang
   *   The source language of the text.
   * @param string $t_lang
   *   The target language for the translation.
   *
   * @return string
   *   The translated text.
   */

  /**
   * Calls the Google API to translate text using server-side key.
   */
  public function translateApiServerCall($text, $s_lang, $t_lang) {
    $config = $this->configFactory->get('auto_translation.settings');
    // Create a new TranslateClient object.
    $client = new \Google\Cloud\Translate\V2\TranslateClient([
      'key' => $config->get('auto_translation_api_key'),
    ]);

    $translation = NULL;

    try {
      $result = $client->translate($text, ['source' => $s_lang, 'target' => $t_lang]);
      $translation = htmlspecialchars_decode($result['text']);
    } catch (RequestException $e) {
      $this->getLogger('auto_translation')->error('Auto translation error: @error', ['@error' => $e->getMessage()]);
    }

    return $translation;
  }

  /**
   * Translates the given text using the API browser call.
   *
   * @param string $text
   *   The text to be translated.
   * @param string $s_lang
   *   The source language of the text.
   * @param string $t_lang
   *   The target language for the translation.
   *
   * @return string
   *   The translated text.
   */
  public function translateApiBrowserCall($text, $s_lang, $t_lang) {
    $translation = NULL;
    $endpoint = 'https://translate.googleapis.com/translate_a/single?client=gtx&sl=' . $s_lang . '&tl=' . $t_lang . '&dt=t&q=' . rawurlencode($text);
    $options = ['verify' => FALSE];

    try {
      $response = $this->httpClient->get($endpoint, $options);
      $data = Json::decode($response->getBody()->getContents());

      $translation = '';
      foreach ($data[0] as $segment) {
        $translation .= $segment[0];
      }
    } catch (RequestException $e) {
      $this->getLogger('auto_translation')->error('Translation API error: @error', ['@error' => $e->getMessage()]);
    }

    return $translation;
  }

  /**
   * Custom function to return saved resources.
   */
  public function getEnabledContentTypes() {
    $config = $this->config();
    $enabledContentTypes = $config->get('auto_translation_content_types') ? $config->get('auto_translation_content_types') : NULL;
    return $enabledContentTypes;
  }

  /**
   * Retrieves the excluded fields.
   *
   * @return array
   *   The excluded fields.
   */
  public function getExcludedFields() {
    $config = $this->config();
    $excludedFields = [
      'uuid',
      'und',
      'published',
      'unpublished',
      '0',
      '1',
      'behavior_settings',
      'draft',
      'ready for review',
      'language',
      'langcode',
      'revision_uid',
      'revision_timestamp',
      'parent_type',
      'parent_field_name',
      'boolean',
      'created',
      'changed',
      'datetime',
      'path',
      'code',
      '#access',
      NULL,
    ];
    $excludedFieldsSettings = $config->get('auto_translation_excluded_fields') ? $config->get('auto_translation_excluded_fields') : NULL;
    if ($excludedFieldsSettings) {
      $excludedFieldsSettings = explode(",", $excludedFieldsSettings);
      $excludedFields = array_merge($excludedFields, $excludedFieldsSettings);
    }
    return $excludedFields;
  }

  /**
   * Implements auto translation for an entity form.
   *
   * Automatically translates translatable fields and nested paragraph fields
   * when adding a translation for an entity.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return array
   *   The modified form array.
   */
  public function formTranslate($form, $form_state) {
    // Ottiene il percorso corrente e le tipologie abilitate per la traduzione.
    $current_path = \Drupal::service('path.current')->getPath();
    $enabledContentTypes = $this->getEnabledContentTypes();
    $entity = $form_state->getFormObject()->getEntity();

    if (!$entity) {
      return $form;
    }
    $entity_type = $entity->getEntityTypeId();
    $bundle = $entity->bundle();

    $excludedFields = $this->getExcludedFields();
    $languageManager = \Drupal::service('language_manager');
    // Proceed only if the current path is for adding translations and the bundle is enabled.
    if ($enabledContentTypes && strpos($current_path, 'translations/add') !== FALSE && in_array($bundle, $enabledContentTypes)) {
      $fields = \Drupal::service('entity_field.manager')->getFieldDefinitions($entity_type, $bundle);
      $d_lang = $this->getTranslationLanguages($entity) ? $this->getTranslationLanguages($entity)['original'] : $languageManager->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();
      $t_lang = $this->getTranslationLanguages($entity) ? $this->getTranslationLanguages($entity)['translated'] : $languageManager->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();

      foreach ($fields as $field) {
        $field_name = $field->getName();
        $field_type = $field->getType();

        // Translation of translatable fields (non-paragraph).
        if ($field->isTranslatable() && !in_array($field_name, $excludedFields)) {
          if (isset($form[$field_name]['widget'])) {
            $this->translateField($entity, $form[$field_name]['widget'], $field_name, $field_type, $d_lang, $t_lang);
          }
        }

        // Translate taxonomy title text
        if (isset($form['name']) && !empty($form['name'])) {
          $this->translateField($entity, $form['name'], $field_name, $field_type, $d_lang, $t_lang);
        }
        // If the field is a reference to paragraphs, handle the translation recursively.
        if ($this->isParagraphReference($field) && !in_array($field_name, $excludedFields)) {
          $this->translateParagraphs($entity, $form, $field_name, $d_lang, $t_lang, $excludedFields);
        }
      }
    }
    return $form;
  }

  /**
   *
   * Handles translation for simple fields, as well as 'link' and 'image' fields.
   *
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity or paragraph.
   * @param array &$widget
   *   The form widget array for the field.
   * @param string $field_name
   *   The name of the field to translate.
   * @param string $field_type
   *   The type of the field.
   * @param string $d_lang
   *   The default language code.
   * @param string $t_lang
   *   The target language code.
   */
  private function translateField($entity, &$widget, $field_name, $field_type, $d_lang, $t_lang) {
    foreach ($widget as $key => &$sub_widget) {
      if (is_numeric($key) && is_array($sub_widget)) {
        // List of potentially translatable keys
        $translatable_keys = ['value', '#default_value', 'title'];

        if (isset($sub_widget['#format'])) {
          $sub_widget['value']['#format'] = $entity->get($field_name)->format;
        }
        if (isset($sub_widget['#text_format'])) {
          $sub_widget['value']['#text_format'] = $entity->get($field_name)->format;
        }

        //Translate  all the string fields
        foreach ($translatable_keys as $field) {
          if (isset($sub_widget[$field])) {
            // If the value is an array, look for the #default_value key inside
            if (is_array($sub_widget[$field]) && isset($sub_widget[$field]['#default_value']) && is_string($sub_widget[$field]['#default_value']) && !empty($sub_widget[$field]['#default_value'])) {
              $sub_widget[$field]['#default_value'] = $this->translate($sub_widget[$field]['#default_value'], $d_lang, $t_lang);
            }
            // If the value is a string directly associated with the key
            elseif (is_string($sub_widget[$field]) && !empty($sub_widget[$field])) {
              $sub_widget[$field] = $this->translate($sub_widget[$field], $d_lang, $t_lang);
            }
          }
        }
      }
    }
    //Translate field media alt text, title text
    if (isset($widget[0]["#default_value"]["alt"]) && !empty($widget[0]["#default_value"]["alt"])) {
      $widget[0]["#default_value"]["alt"] = $this->translate($widget[0]["#default_value"]["alt"], $d_lang, $t_lang);
    }
    if (isset($widget[0]["#default_value"]["title"]) && !empty($widget[0]["#default_value"]["title"])) {
      $widget[0]["#default_value"]["title"] = $this->translate($widget[0]["#default_value"]["title"], $d_lang, $t_lang);
    }
  }

  /**
   * Checks if a field is a reference to a paragraph.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field
   *   The field definition.
   *
   * @return bool
   *   TRUE if the field points to a paragraph, FALSE otherwise.
   */
  private function isParagraphReference($field) {
    return ($field->getSetting('target_type') === 'paragraph');
  }

  /**
   * Handles the translation of paragraph fields recursively.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The main entity.
   * @param array &$form
   *   The form array.
   * @param string $field_name
   *   The name of the field containing the paragraphs.
   * @param string $d_lang
   *   The default language code.
   * @param string $t_lang
   *   The target language code.
   * @param array $excludedFields
   *   Array of field names to exclude from translation.
   */
  private function translateParagraphs($entity, &$form, $field_name, $d_lang, $t_lang, $excludedFields) {
    $paragraphItems = $entity->get($field_name);
    foreach ($paragraphItems as $index => $paragraphItem) {
      if ($paragraphItem->entity instanceof \Drupal\paragraphs\ParagraphInterface) {
        if (isset($form[$field_name]['widget'][$index]['subform'])) {
          $this->processParagraphTranslation($paragraphItem->entity, $form[$field_name]['widget'][$index]['subform'], $d_lang, $t_lang, $excludedFields);
        }
      }
    }
  }

  /**
   * Processes the translation of a paragraph entity recursively.
   *
   * For each translatable field of the paragraph (and nested paragraphs),
   * the translation is performed, and at the end, the translated entity is saved.
   *
   * @param \Drupal\paragraphs\ParagraphInterface $paragraphEntity
   *   The paragraph entity.
   * @param array &$form
   *   The subform array related to the paragraph.
   * @param string $d_lang
   *   The default language code.
   * @param string $t_lang
   *   The target language code.
   * @param array $excludedFields
   *   Array of field names to exclude.
   */
  private function processParagraphTranslation($paragraphEntity, &$form, $d_lang, $t_lang, $excludedFields) {
    if (!$paragraphEntity instanceof \Drupal\paragraphs\ParagraphInterface) {
      return;
    }

    // Iterate over each field of the paragraph.
    foreach ($paragraphEntity->getFields() as $field_name => $field) {
      // If the field is translatable and not among the excluded ones, proceed.
      if ($field->getFieldDefinition()->isTranslatable() && !in_array($field_name, $excludedFields)) {
        if (isset($form[$field_name]['widget'])) {
          $this->translateField($paragraphEntity, $form[$field_name]['widget'], $field_name, $field->getFieldDefinition()->getType(), $d_lang, $t_lang);
        }
      }
      // If the field is a reference to paragraphs, handle it recursively.
      if ($this->isParagraphReference($field)) {
        $nestedItems = $paragraphEntity->get($field_name);
        foreach ($nestedItems as $idx => $nestedItem) {
          if ($nestedItem->entity instanceof \Drupal\paragraphs\ParagraphInterface) {
            if (isset($form[$field_name]['widget'][$idx]['subform'])) {
              $this->processParagraphTranslation($nestedItem->entity, $form[$field_name]['widget'][$idx]['subform'], $d_lang, $t_lang, $excludedFields);
            }
          }
        }
      }
    }
  }

  /**
   * Custom get string between function.
   */
  public function getStringBetween($string, $start, $end) {
    $string = ' ' . $string;
    $ini = strpos($string, $start);
    if ($ini == 0) {
      return '';
    }
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    return substr($string, $ini, $len);
  }

  /**
   * Retrieves the container.
   *
   * @return mixed
   *   The container.
   */
  public static function getContainer() {
    return \Drupal::getContainer();
  }

  /**
   * Retrieves the configuration settings.
   *
   * @return object
   *   The configuration settings.
   */
  public static function config() {
    return static::getContainer()
      ->get('config.factory')
      ->get('auto_translation.settings');
  }
  /**
   * Retrieves the source and target languages for an entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The translated entity.
   *
   * @return array
   *   An array with 'original' and 'translated' containing the respective languages.
   */
  private function getTranslationLanguages(ContentEntityInterface $entity) {
    $language_manager = \Drupal::service('language_manager');
    $route_match = \Drupal::routeMatch();

    // Try to get the languages directly from the URL.
    $original_language = $route_match->getParameter('source') ?? NULL;
    $translated_language = $route_match->getParameter('target') ?? NULL;

    // If we find the languages in the URL, use them directly and in the correct order.
    if (!empty($original_language) && !empty($translated_language)) {
      return [
        'original' => (string) $original_language->getId(),   // The first language from the URL
        'translated' => (string) $translated_language->getId(), // The second language from the URL
      ];
    }

    // If the entity is not translatable, use only the current language.
    if (!$entity->getEntityType()->isTranslatable()) {
      return [
        'original' => $entity->language()->getId(),
        'translated' => $language_manager->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId(),
      ];
    }

    // Find the original language of the entity.
    $default_language = $entity->getUntranslated()->language()->getId();

    // Current language = the language we are translating into.
    $current_language = $language_manager->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();

    return [
      'original' => $default_language, // The base language of the entity
      'translated' => $current_language, // The language we are translating into
    ];
  }
  /**
   * Retrieves the specified form.
   *
   * @param object $messages
   *   The json of the message to retrieve.
   *
   * @return mixed
   *   The service object if found, null otherwise.
   */
  public static function getMessages($messages) {
    return static::getContainer()
      ->get('messenger')->addMessage(t('Auto translation error: @error', [
        '@error' => Markup::create(htmlentities(json_encode($messages))),
      ]), MessengerInterface::TYPE_ERROR);
  }

  /**
   * Returns the path of the module.
   *
   * @return string
   *   The path of the module.
   */
  public static function getModulePath() {
    return static::getContainer()
      ->get('extension.list.module')
      ->getPath('auto_translation');
  }

  /**
   * Retrieves the specified module by name.
   *
   * @param string $module_name
   *   The name of the module to retrieve.
   *
   * @return mixed|null
   *   The module object if found, null otherwise.
   */
  public static function getModule($module_name) {
    return static::getContainer()
      ->get('extension.list.module')
      ->get($module_name);
  }
}
